@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach($points as $point)
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{$point['name']}}</div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    @if (isset($point['image']))
                                        <img src="{{url('storage/pointImages/'.$point['image'])}}"
                                             class="img img-responsive center-block"/>
                                    @else
                                        <img src="http://via.placeholder.com/100x100"
                                             class="img img-responsive center-block"/>
                                    @endif
                                </div>
                                <div class="col-md-10">
                                    <p>{{$point['description']}}</p>
                                    <button type="button" class="btn btn-danger pull-right" data-toggle="modal"
                                            data-target="#myModal" data-id="{{$point['id']}}">
                                        DELETE
                                    </button>
                                    <a class="btn btn-primary pull-right" href="{{url('edit_point/'.$point['id'])}}">EDIT</a>
                                    <a class="btn btn-default pull-right"
                                       href="{{url('point/'.$point['id'])}}">DETAILS</a>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach

            <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete point?</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure you want to delete this point?</p>
                            </div>
                            <div class="modal-footer">
                                <a id="deleteButton" href="#" class="btn btn-danger">DELETE</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">DON'T</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row text-center">
                    {{$points->links()}}
                </div>
            </div>
        </div>
    </div>
    @include('addPointButton');

    <script src="{{asset('js/modal.js')}}"></script>
@endsection
