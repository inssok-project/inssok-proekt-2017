@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" enctype="multipart/form-data" action="{{!isset($point) ? route('point.store') : action('HomeController@updatePoint', ['id' => $point['id']])}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">
                            Name
                        </label>
                        <input name="name" class="form-control" type="text" required
                               value="{{isset($point) ? $point['name'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="description">
                            Description
                        </label>
                        <textarea name="description" class="form-control" required>{{isset($point) ? $point['description'] : ""}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="latitude">
                            Latitude
                        </label>
                        <input name="latitude" class="form-control" type="number" step="any" required
                               value="{{isset($point) ? $point['latitude'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="longitude">
                            Longitude
                        </label>
                        <input name="longitude" class="form-control" type="number" step="any" required
                               value="{{isset($point) ? $point['longitude'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="width">
                            Width
                        </label>
                        <input name="width" class="form-control" type="number" step="any" required
                               value="{{isset($point) ? $point['width'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="length">
                            Length
                        </label>
                        <input name="length" class="form-control" type="number" step="any" required
                               value="{{isset($point) ? $point['length'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="height">
                            Height
                        </label>
                        <input name="height" class="form-control" type="number" step="any" required
                               value="{{isset($point) ? $point['height'] : ""}}"/>
                    </div>
                    <div class="form-group">
                        <label for="image">
                            Image
                        </label>
                        <input name="image" type="file"/>
                    </div>
                    <button type="submit" class="btn btn-success">SAVE</button>
                </form>
            </div>
        </div>
    </div>
@endsection
