@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>{{$point['name']}}</h1>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        @if (isset($point['image']))
                            <img src="{{url('storage/pointImages/'.$point['image'])}}"
                                 class="img img-responsive center-block"/>
                        @else
                            <img src="http://via.placeholder.com/100x100"
                                 class="img img-responsive center-block"/>
                        @endif
                    </div>
                </div>
                <div class="row text-center">
                    <p>{{$point['description']}}</p>
                    <div class="col-md-6 text-right">
                        <p>Latitude</p>
                        <p>Longitude</p>
                        <p>Height</p>
                        <p>Width</p>
                        <p>Length</p>
                    </div>
                    <div class="col-md-6 text-left">
                        <p>{{$point['latitude']}}</p>
                        <p>{{$point['longitude']}}</p>
                        <p>{{$point['height']}}</p>
                        <p>{{$point['width']}}</p>
                        <p>{{$point['length']}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete point?</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this point?</p>
                </div>
                <div class="modal-footer">
                    <a href="{{url('delete_point/'.$point['id'])}}" class="btn btn-danger">DELETE</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">DON'T</button>
                </div>
            </div>

        </div>
    </div>
    @if (Auth::check() && Auth::user()->id == $point['user_id'])
        @include('deletePointButton');
        @include('editPointButton');
    @endif
    @include('addPointButton');
@endsection
