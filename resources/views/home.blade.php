@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Dashboard</div>--}}

                {{--<div class="panel-body">--}}
                {{--@if (session('status'))--}}
                {{--<div class="alert alert-success">--}}
                {{--{{ session('status') }}--}}
                {{--</div>--}}
                {{--@endif--}}

                {{--You are logged in!--}}
                {{--</div>--}}
                {{--</div>--}}
                @foreach($points as $point)
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{$point['name']}}</div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    @if (isset($point['image']))
                                        <img src="{{url('storage/pointImages/'.$point['image'])}}"
                                             class="img img-responsive center-block"/>
                                    @else
                                        <img src="http://via.placeholder.com/100x100"
                                             class="img img-responsive center-block"/>
                                    @endif
                                </div>
                                <div class="col-md-10">
                                    <p>{{$point['description']}}</p>
                                    <a class="btn btn-primary pull-right" href="{{url('point/'.$point['id'])}}">DETAILS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row text-center">
                    {{$points->links()}}
                </div>
            </div>
        </div>
    </div>
    @include('addPointButton');
@endsection
