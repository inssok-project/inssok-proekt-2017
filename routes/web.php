<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//API
Route::get('pois', 'APIController@index')->name('pois.index');
Route::get('pois/{id}', 'APIController@show')->name('pois.show');
//Route::post('pois', 'APIController@store')->name('pois.store');
//Route::delete('pois/{id}', 'APIController@destroy')->name('pois.destroy');
//Route::patch('pois/{id}', 'APIController@update')->name('pois.update');
Route::get('pois_by_distance', 'APIController@closestPoints')->name('pois.byDistance');

//AUTH
Auth::routes();

//FRONT END
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('point/{id}', 'HomeController@point')->name('home.point');
Route::group(['middleware' => 'auth'], function() {
    Route::get('points_from_user', 'HomeController@pointsFromUser')->name('point.fromUser');
    Route::get('new_point', 'HomeController@newPoint')->name('point.new');
    Route::post('store_new_point', 'HomeController@storeNewPoint')->name('point.store');
    Route::get('edit_point/{id}', 'HomeController@editPoint')->name('point.edit');
    Route::post('update_point/{id}', 'HomeController@updatePoint')->name('point.update');
    Route::get('delete_point/{id}', 'HomeController@deletePoint')->name('point.delete');
});
