<?php
/**
 * Created by PhpStorm.
 * User: ACER
 * Date: 05.1.2018
 * Time: 14:10
 */

use App\PointOfInterest;

$distances = [];

function getClosestPoints($originLatitude, $originLongitude, $maxDistance, $numberOfPoints)
{
    $pois = PointOfInterest::query()->get()->toArray();

    $result = array_filter($pois, function ($element) use ($originLatitude, $originLongitude, $maxDistance) {
        $distance = distanceBetween($originLatitude, $originLongitude, $element['latitude'], $element['longitude']);
        $GLOBALS['distances'][$element['id']] = $distance;
        return $distance <= $maxDistance;
    });

    usort($result, function ($el1, $el2) {
        return $GLOBALS['distances'][$el1['id']] > $GLOBALS['distances'][$el2['id']];
    });

    $result = array_slice($result, 0, $numberOfPoints);

    return $result;
}

function distanceBetween($originLatitude, $originLongitude, $destinationLatitude, $destinationLongitude)
{
    return haversineGreatCircleDistance($originLatitude, $originLongitude, $destinationLatitude, $destinationLongitude);
}

function haversineGreatCircleDistance(
    $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
}