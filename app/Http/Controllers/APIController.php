<?php

namespace App\Http\Controllers;

use App\PointOfInterest;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function index()
    {
        return PointOfInterest::query()->orderBy('id', 'desc')->get();
    }

    public function show($id)
    {
        return PointOfInterest::query()->findOrFail(['id' => $id]);
    }

    public function store(Request $request)
    {
        $poi = new PointOfInterest($request->input());
        $poi->save();
    }

    public function destroy($id)
    {
        PointOfInterest::destroy($id);
    }

    public function update($id, Request $request)
    {
        $oldPoi = PointOfInterest::query()->findOrFail(['id' => $id])->first();
        $oldPoi->update($request->input());
        $oldPoi->save();
    }

    public function closestPoints(Request $request)
    {
        if (!$request->has(['lat', 'lng', 'maxDistance', 'numberOfPoints'])) {
            return response('Invalid request. Query parameters lat, lng, maxDistance and numberOfPoints must be present.', 403);
        }

        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $maxDistance = $request->get('maxDistance');
        $numberOfPoints = $request->get('numberOfPoints');

        $result = getClosestPoints($lat, $lng, $maxDistance, $numberOfPoints);
        return $result;
    }
}
