<?php

namespace App\Http\Controllers;

use App\PointOfInterest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $points = PointOfInterest::query()->orderBy('id', 'desc')->paginate(env('POINTS_PER_PAGE'));
        return view('home')->with('points', $points);
    }

    public function point($id)
    {
        $point = PointOfInterest::query()->where(['id' => $id])->first();
        return view('point')->with('point', $point);
    }

    public function pointsFromUser()
    {
        $id = Auth::user()->id;
        $pointsFromUser = PointOfInterest::query()->where(['user_id' => $id])->orderBy('id', 'desc')->paginate(env('POINTS_PER_PAGE'));
        return view('pointsFromUser')->with('points', $pointsFromUser);
    }

    public function newPoint()
    {
        return view('editPoint');
    }

    public function storeNewPoint(Request $request)
    {
        $point = PointOfInterest::make($request->input());
        $point->user_id = Auth::user()->id;
        $point->save();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $point['id'] . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/pointImages', $imageName);
            PointOfInterest::query()->where(['id' => $point['id']])->update(['image' => $imageName]);
        }

        return redirect()->route('home.point', ['id' => $point['id']]);
    }

    public function editPoint($id)
    {
        $point = PointOfInterest::query()->where(['id' => $id])->first();
        if ($point['user_id'] != Auth::user()->id) {
            return redirect()->route('point.fromUser');
        }
        return view('editPoint')->with('point', $point);
    }

    public function updatePoint($id, Request $request)
    {
        $point = PointOfInterest::query()->where(['id' => $id])->first();
        if ($point['user_id'] != Auth::user()->id) {
            return redirect()->route('point.fromUser');
        }

        if (isset($point['image']) && $request->hasFile('image')) {
            $imageName = $point['image'];
            Storage::delete('public/pointImages/' . $imageName);
        }

        DB::table('point_of_interests')->where(['id' => $id])->update($request->except('_token'));

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $point['id'] . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/pointImages', $imageName);
            PointOfInterest::query()->where(['id' => $point['id']])->update(['image' => $imageName]);
        }

        return redirect()->route('home.point', ['id' => $point['id']]);
    }

    public function deletePoint($id)
    {
        $point = PointOfInterest::query()->where(['id' => $id])->first();
        if ($point['user_id'] != Auth::user()->id) {
            return redirect()->route('point.fromUser');
        }
        if (isset($point['image'])) {
            $imageName = $point['image'];
            Storage::delete('public/pointImages/' . $imageName);
        }

        DB::table('point_of_interests')->where(['id' => $id])->delete();

        return redirect()->route('point.fromUser');
    }
}
