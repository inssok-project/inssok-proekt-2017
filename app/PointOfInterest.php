<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointOfInterest extends Model
{
    protected $fillable = [
        'name', 'description', 'latitude', 'longitude', 'width', 'length', 'height'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
