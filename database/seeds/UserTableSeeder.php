<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $user = new User([
            'name' => 'feko',
            'email' => 'feko@feko.com',
            'password' => bcrypt('feko'),
            'remember_token' => str_random(10)
        ]);

        $user->save();

        factory(\App\PointOfInterest::class, 3)->make()->each(function (\App\PointOfInterest $poi) use ($user){
            $user->pointsOfInterest()->save($poi);
        });
    }
}
