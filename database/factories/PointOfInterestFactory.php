<?php

use Faker\Generator as Faker;

$factory->define(App\PointOfInterest::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(5),
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'width' => $faker->numberBetween(1, 10),
        'height' => $faker->numberBetween(1, 10),
        'length' => $faker->numberBetween(1, 10)
    ];
});
