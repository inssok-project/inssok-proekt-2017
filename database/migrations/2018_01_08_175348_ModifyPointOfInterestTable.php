<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPointOfInterestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_of_interests', function (Blueprint $table) {
            $table->decimal('width', 10, 1)->change();
            $table->decimal('length', 10, 1)->change();
            $table->decimal('height', 10, 1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_of_interests', function (Blueprint $table) {
            $table->decimal('width', 4, 1)->change();
            $table->decimal('length', 4, 1)->change();
            $table->decimal('height', 4, 1)->change();
        });
    }
}
