<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointOfInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_of_interests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description', 500)->nullable();
            $table->string('image')->nullable();
            $table->double('latitude', 20, 17);
            $table->double('longitude', 20, 17);
            $table->double('width', 4, 1);
            $table->double('length', 4, 1);
            $table->double('height', 4, 1);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_of_interests');
    }
}
